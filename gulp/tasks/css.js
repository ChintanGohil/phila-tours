var gulp = require("gulp"),
    autoprefixer = require("autoprefixer"),
    cssvars = require("postcss-simple-vars"),
    postcss = require("gulp-postcss"),
    nested = require("postcss-nested"),
    cssImport = require("postcss-import"),
    mixins = require("postcss-mixins");

gulp.task('css', function(){
    return gulp.src("./app/assets/styles/styles.css")
        .pipe(postcss([cssImport,mixins,cssvars,nested,autoprefixer]))
//        .on('error' , function(errorInfo){
//        
//        })
        .pipe(gulp.dest("./app/css"));
});